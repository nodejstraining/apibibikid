var jwt 	= require('jsonwebtoken');
var User 	= require('../models/user');

module.exports.authenticate = function(req,res){
	User.findOne({email : req.body.email}, function(err, user){
		if(err) throw err;
		if(!user){
			res.json({success: false, message: 'Authentication failed. User not found.'});
		}else{
			if(user.password != req.body.password){
				res.json({success: false, message: 'Authentication failed. Wrong password.'});
			}else{
				var token = jwt.sign(user, process.env.SECRET_KEY, {
					expiresIn: 4000
				});
				res.json({
					seccess: true,
					token: token
				})
			}
		}
	})
}