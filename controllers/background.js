var Background 	= require('../models/background');

var backgrounds = {
	getAll: function(req,res){
		Background.find()
		.exec(function(err, data){
			if(err) throw err
			res.json({data: data})
		})
	},
	getOne: function(req,res){
		var backgroundId = req.params.id;
		Background.find({'_id' : backgroundId})
		.exec(function(err, data){
			if(err) throw err
			res.json({data: data})
		})
	},
	create: function(req,res){
		var newBackground = new Background({
			title : req.body.title,
			url : req.body.url
		})
		newBackground.save(function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	},
	update: function(req,res){
		var backgroundId = req.params.id;
		var url = req.body.url;
		Background.update({'_id' : backgroundId}, {'url':url}, function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	},
	delete: function(req,res){
		var backgroundId = req.params.id;
		Background.deleteOne({'_id' : backgroundId}, function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	}
}
module.exports = backgrounds;