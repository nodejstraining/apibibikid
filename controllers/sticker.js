var Sticker 	= require('../models/sticker');

var stickers = {
	getAll: function(req,res){
		Sticker.find()
		.exec(function(err, data){
			if(err) throw err
			res.json({data: data})
		})
	},
	getOne: function(req,res){
		var stickerId = req.params.id;
		Sticker.find({'_id' : stickerId})
		.exec(function(err, data){
			if(err) throw err
			res.json({data: data})
		})
	},
	create: function(req,res){
		var newSticker = new Sticker({
			title : req.body.title,
			url : req.body.url
		})
		newSticker.save(function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	},
	update: function(req,res){
		var stickerId = req.params.id;
		var url = req.body.url;
		Sticker.update({'_id' : stickerId}, {'url':url}, function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	},
	delete: function(req,res){
		var stickerId = req.params.id;
		Sticker.deleteOne({'_id' : stickerId}, function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	}
}
module.exports = stickers;