var Story = require('../models/story');

//Thêm truyện
var storys = {
	//Trả về tất cả thông tin không bao gồm nội dung của truyện
	getAll : function(req,res){
		Story.find({},{'name_story':1, 'cover_story' : 1, 'author':1, 'price':1})
		.exec(function(err, data){
			if(err) throw err
			res.json({data: data})
		})
	},
	//Trả về tất cả thông tin của 1 truyện theo id của truyện
	getOne : function(req,res){
		var storyId = req.params.id;
		Story.find({'_id' : storyId})
		.exec(function(err, data){
			if(err) throw err
			res.json({data: data})
		})
	},
	create : function(req,res){
		var newStory = new Story({
			name_story: req.body.name_story,
			author: req.body.author,
			cover_story: req.body.cover_story,
			price: req.body.price,
			content: req.body.content
		})
		newStory.save(function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	},
	update : function(req,res){
		var storyId = req.params.id;
		var content = req.body.content;
		Story.update({'_id' : storyId}, {'content':content}, function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	},
	delete : function(req,res){
		var storyId = req.params.id;
		Story.deleteOne({'_id' : storyId}, function(err){
				if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	}
}

module.exports = storys;


