var Character 	= require('../models/character');

var characters = {
	getAll: function(req,res){
		Character.find()
		.exec(function(err, data){
			if(err) throw err
			res.json({data: data})
		})
	},
	getOne: function(req,res){
		var characterId = req.params.id;
		Character.find({'_id' : characterId})
		.exec(function(err, data){
			if(err) throw err
			res.json({data: data})
		})
	},
	create: function(req,res){
		var newCharacter = new Character({
			title : req.body.title,
			url : req.body.url
		})
		newCharacter.save(function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	},
	update: function(req,res){
		var characterId = req.params.id;
		var url = req.body.url;
		Character.update({'_id' : characterId}, {'url':url}, function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	},
	delete: function(req,res){
		var characterId = req.params.id;
		Character.deleteOne({'_id' : characterId}, function(err){
			if(err){
				res.json({message: err})
			}else{
				res.json({message: 'success'})
			}
		})
	}
}
module.exports = characters;