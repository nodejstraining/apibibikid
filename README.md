What is this repository for?
Tr? v? api cho ?ng d?ng BiBi Kid

How do I get set up?
Contribution guidelines
Writing tests 
Running post localhost:3000/secure-api/authenticate with: email : ngannt1710@gmail.com password : 123 => Result : token
Running Get in file app.js Ex: localhost:3000/api/backgrounds?token=token(ket qua tra ve o tren)
								localhost:3000/api/characters?token=token(ket qua tra ve o tren)
								localhost:3000/api/stickers?token=token(ket qua tra ve o tren)
								localhost:3000/api/storys?token=token(ket qua tra ve o tren)

secureRoutes.route('/backgrounds')
	.get(backgroundController.getAll)
	.post(backgroundController.create)
	.put()
	.delete();

secureRoutes.route('/backgrounds/:id')
	.get(backgroundController.getOne)
	.post()
	.put(backgroundController.update)
	.delete(backgroundController.delete)

secureRoutes.route('/characters')
	.get(characterController.getAll)
	.post(characterController.create)
	.put()
	.delete();

secureRoutes.route('/characters/:id')
	.get(characterController.getOne)
	.post()
	.put(characterController.update)
	.delete(characterController.delete)

secureRoutes.route('/stickers')
	.get(stickerController.getAll)
	.post(stickerController.create)
	.put()
	.delete();

secureRoutes.route('/stickers/:id')
	.get(stickerController.getOne)
	.post()
	.put(stickerController.update)
	.delete(stickerController.delete)

secureRoutes.route('/storys')
	.get(storyController.getAll)
	.post(storyController.create)
	.put()
	.delete();

secureRoutes.route('/storys/:id')
	.get(storyController.getOne)
	.post()
	.put(storyController.update)
	.delete(storyController.delete)

Who do I talk to?
Repo owner or admin
Other community or team contact