var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Schema_Sticker = new Schema({
	title: String,
	url : String,
})

module.exports = mongoose.model('Sticker',Schema_Sticker);