var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Schema_Story = new Schema({
	name_story : String,
	cover_story : String,
	author: String,
	content: String,
	price : Number

})

module.exports = mongoose.model('Story', Schema_Story);