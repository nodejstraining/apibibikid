var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Schema_Character = new Schema({
	title : String,
	url : String,
})

module.exports = mongoose.model('Character',Schema_Character);