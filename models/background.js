var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Schema_Background = new Schema({
	title: String, //Tên của chủ đề
	url : String,
})

module.exports = mongoose.model('Background', Schema_Background);
