var express 	= require('express');
var bodyParser 	= require('body-parser');
var app 		= express();
var mongoose 	= require('mongoose');
var jwt 		= require('jsonwebtoken');
var server = require('http').createServer(app);
var secureRoutes = express.Router();


//controllers
var backgroundController = require('./controllers/background')
var characterController = require('./controllers/character')
var stickerController = require('./controllers/sticker')
var storyController = require('./controllers/story')
var authenticateController = require('./controllers/authenticate-controller')

process.env.SECRET_KEY = 'mybadasskey';

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use('/api', secureRoutes);

var config = require('./models/config');
config.setConfig();
mongoose.connect(process.env.MONGOOSE_CONNECT);

app.post('/secure-api/authenticate', authenticateController.authenticate);

//Validation middleware
secureRoutes.use(function(req,res,next){
	var token = req.body.token || req.query.token || req.headers['token'];
	if(token){
		jwt.verify(token, process.env.SECRET_KEY, function(err,decode){
			if(err){
				res.status(500).send('Invalid Token');
			}else{
				next();
			}
		})
	}else{
		res.send('Please send a token');
	}
})

secureRoutes.route('/backgrounds')
	.get(backgroundController.getAll)
	.post(backgroundController.create)
	.put()
	.delete();

secureRoutes.route('/backgrounds/:id')
	.get(backgroundController.getOne)
	.post()
	.put(backgroundController.update)
	.delete(backgroundController.delete)

secureRoutes.route('/characters')
	.get(characterController.getAll)
	.post(characterController.create)
	.put()
	.delete();

secureRoutes.route('/characters/:id')
	.get(characterController.getOne)
	.post()
	.put(characterController.update)
	.delete(characterController.delete)

secureRoutes.route('/stickers')
	.get(stickerController.getAll)
	.post(stickerController.create)
	.put()
	.delete();

secureRoutes.route('/stickers/:id')
	.get(stickerController.getOne)
	.post()
	.put(stickerController.update)
	.delete(stickerController.delete)

secureRoutes.route('/storys')
	.get(storyController.getAll)
	.post(storyController.create)
	.put()
	.delete();

secureRoutes.route('/storys/:id')
	.get(storyController.getOne)
	.post()
	.put(storyController.update)
	.delete(storyController.delete)


var port = process.env.PORT || 3000;
server.listen(port, function () {
    console.log("server is runing on localhost:3000");
});



